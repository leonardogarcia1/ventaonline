
(function(root,factory) {
    root.notify = factory();
})(typeof global !== 'undefined' ? global : this.window || this.global, function(root) {
    var init = function(root) {

        var notify = function() {
          
            this.notify = {};

            this.notifys = [];

            this.success = false;

            this.options = {
                ele: "body",
                offset: {
                    from: "top",
                    amount: 20
                },
                align: "right",
                width: 250,
                delay: 5000,
                allow_dismiss: true,
                stackup_spacing: 10,
                text: "Notification Message Here",
                icon: "times-circle",
                status: "danger",
                alertClass: "", 
                fnStart: false, 
                fnEnd: false, 
                fnEndHide: false, 
            };
        };

        notify.prototype.send = function(text, status) {
          
            if (typeof this.options.fnStart == "function")
                this.options.fnStart();

          
            var self = this;
          
            var flag = 1;
          
            var i = +this.notifys.length - 1;
         
            var parent;

            var notify = this.generate(text, status);

            if (this.notifys.length) {
            
            while (i >= 0 && flag) {
              
                if (this.notifys[i].text == notify.text && this.notifys[i].status == notify.status) {
                
                    parent = this.notifys[i];
               
                    flag = 0;
               
                    notify.elem.css("top", parseInt(parent.elem.css("top")) + 4);
                    notify.elem.css("right", parseInt(parent.elem.css("right")) + 4);
                }
                i--;
                }
            }

            notify.posTop = parseInt(notify.elem.css("top"));

            if (typeof parent == 'object') {
          
                clearTimeout(parent.timer);
            
                parent.timer = setTimeout(function() {
                    self.closed(parent);
                }, this.options.delay);
            
                parent.notifyGroup.push(notify);
            
            } else {
            
                notify.timer = setTimeout(function() {
                    self.closed(notify);
                }, 
                this.options.delay);
                this.notifys.push(notify);
            }

            notify.elem.fadeIn();

          
            if (typeof this.options.fnEnd == "function")
                this.options.fnEnd();
            }

            notify.prototype.closed = function(notify) {
                var self = this;
                var idx, i, move, next;

                if (this.notifys !== null) {
                    idx = $.inArray(notify, this.notifys);

                    if (notify.notifyGroup !== "undefined" && notify.notifyGroup.length) {
                        for (i = 0; i < notify.notifyGroup.length; i++) {
                            $(notify.notifyGroup[i].elem).alert("close");
                        }
                    }

                    $(this.notifys[idx].elem).alert("close");

                    if (idx !== -1) {
                        if (this.notifys.length > 1) {
                            next = idx + 1;
                            move = this.notifys[next].posTop - this.notifys[idx].posTop;

                            for (i = idx; i < this.notifys.length; i++) {
                                this.animate(self.notifys[i], parseInt(self.notifys[i].posTop) - move);
                                self.notifys[i].posTop = parseInt(self.notifys[i].posTop) - move
                            }
                        }

                        this.notifys.splice(idx, 1);

                        if (typeof this.options.fnEndHide == "function")
                            this.options.fnEndHide();
                    }
                }
            }

            notify.prototype.animate = function(hullabaloo, move) {
                var timer,
                top, 
                i, 
                group = 0; 

                top = parseInt(notify.elem.css("top"));
                group = notify.notifyGroup.length;

                timer = setInterval(frame, 2);
                 function frame() {
                    if (top == move) {
                        clearInterval(timer);
                    } else {
                        top--;
                        notify.elem.css("top", top);
                        if (group) {
                            for (i = 0; i < group; i++) {
                                notify.notifyGroup[i].elem.css("top", top + 5);
                            }
                        }
                    }
                }
            }

            notify.prototype.generate = function(text, status) {
                var alertsObj = {
                    icon: "", 
                    status: status || this.options.status, 
                    text: text || this.options.text, 
                    elem: $("<div>"), 

                    notifyGroup: []
                };
                var option, 
                offsetAmount, 
                css; 

                option = this.options;

                alertsObj.elem.attr("class", "hullabaloo alert "+option.alertClass);

                alertsObj.elem.addClass("alert-" + alertsObj.status);

                if (option.allow_dismiss) {
                    alertsObj.elem.addClass("alert-dismissible");
                    alertsObj.elem.append("<button  class=\"close\" data-dismiss=\"alert\" type=\"button\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>");
                }

                // Icon
                if (alertsObj.status == "success")
                    alertsObj.icon = "check";
                else if (alertsObj.status == "info-circle")
                    alertsObj.icon = "info";
                else if (this.notify.status == "danger")
                    alertsObj.icon = "times-circle";
                else if (this.notify.status == "warning")
                    alertsObj.icon = "exclamation-triangle";
                else
                    alertsObj.icon = option.icon;

                alertsObj.elem.append('<i class="fa fa-' + alertsObj.icon + '"></i> ' + alertsObj.text);

                offsetAmount = option.offset.amount;
            
                $(".hullabaloo").each(function() {
                    return offsetAmount = Math.max(offsetAmount, parseInt($(this).css(option.offset.from)) + $(this).outerHeight() + option.stackup_spacing);
                });

                css = {
                    "position": (option.ele === "body" ? "fixed" : "absolute"),
                    "margin": 0,
                    "z-index": "9999",
                    "display": "none"
                };
                css[option.offset.from] = offsetAmount + "px";
                alertsObj.elem.css(css);

                if (option.width !== "auto") {
                    alertsObj.elem.css("width", option.width + "px");
                }
                $(option.ele).append(alertsObj.elem);
                switch (option.align) {
                case "center":
                    alertsObj.elem.css({
                        "left": "50%",
                        "margin-left": "-" + (alertsObj.elem.outerWidth() / 2) + "px"
                    });
                    break;
                case "left":
                    alertsObj.elem.css("left", "20px");
                    break;
                default:
                alertsObj.elem.css("right", "20px");
            }

            return alertsObj;
        };

        return notify;
    };
    return init(root);
});
